import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Location } from '@angular/common';

@Component({
  selector: 'app-extendedsearch',
  templateUrl: './extendedsearch.component.html',
  styleUrls: ['./extendedsearch.component.sass']
})
export class ExtendedsearchComponent implements OnInit {

  seesion_key_data = "";
  extendedSearchform: FormGroup;
  returnUrl: string;
  loading = false;
  submitted = false;
  phonecode = [];
  token_data = "";
  uid_data= "";
  resonseError="";
  search_data = "";
  searchText = "";
  searchNotes ="";
  loader=false;
  not_found = "";
  countrydiscovery ="";
  countryorigin= "";
  countryorigin_val="";
  countrydiscovery_val="";

 constructor(
  private formBuilder: FormBuilder,
  private http: HttpClient,
  private router: Router,
  private route: ActivatedRoute
  ) { 

    this.getJSON().subscribe(data => {
        this.phonecode = data.countries;
    });
}

   ngOnInit() {
    this.search_data = this.route.snapshot.params.token;
    this.seesion_key_data = localStorage.getItem("key_session");  
         if(this.seesion_key_data === "null" || this.seesion_key_data === "" || this.seesion_key_data === null)
           {
           this.router.navigate(['login']);
           }
    this.extendedSearchform = this.formBuilder.group({
            searchText: ['',Validators.required],
            searchNotes:"",
        });

    if(this.search_data){
      this.loader=true;
      this.search(this.search_data,this.seesion_key_data);
    }
  }


  public getJSON(): Observable<any> {
        return this.http.get("./assets/phonecode.json");
    }

   
  get f() { return this.extendedSearchform.controls; }

  

      load() 
      {
        if(this.route.snapshot.params.token){
          this.router.navigate(['extendedsearch']);
        }else{
          location.reload();
        }         
       }

      onGoToThankyou()
      {
        this.router.navigate(['thankyou']);
      }  

   selectOptionCountryorigin(id: string) {
      this.countryorigin = id;
      this.countryorigin_val="";
      this.countrydiscovery_val="";
    }

   selectOptionCountry(id: string) {
     this.countrydiscovery = id;
     this.countrydiscovery_val="";
     this.countryorigin_val="";   
    }

  onSubmit_search()
  {     
        this.submitted = true;
        // stop here if form is invalid      
      if (this.extendedSearchform.invalid){
        return;     
         }

      if(this.countryorigin =="" && this.countrydiscovery=="")
        
          { 
            if(this.countryorigin =="")
              {
                this.countryorigin_val='1';
                this.countrydiscovery_val='1';
                this.not_found="3";
             return;
             }
             if(this.countrydiscovery==""){
              this.countrydiscovery_val='1';
              this.countryorigin_val='1';
              this.not_found="3";             
              return;
             }
        }
        else
        {
           this.loader=true;
           this.resonseError = "";
           this.seesion_key_data = localStorage.getItem("key_session");
           var data = this.f.searchText.value
           this.search(data,this.seesion_key_data);           
          }
  
    } 

    search(data,seesion_key_data){
      this.loader=true;
        const httpOptions = {
              headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'Authorization': 'Token '+seesion_key_data,
              })
            };

      this.http.get<any>(`https://acc-lookup.sjerlok.com:8081/api/lookup/${data}/`,httpOptions)
                  .subscribe(data => 
                  {
             if(data.number_hit_searches == "0")
              {
               this.not_found = "1" ;
              }

             if(data.number_hit_searches != "0")
              {
               this.not_found = "2" ;
              }
              this.loader=false;             
                  },
                   error => 
                   {
                    this.loader=false;
                if ((error.status) == 404) {
                   this.not_found= "404";
                }            
              }
          );
    }
    
}
