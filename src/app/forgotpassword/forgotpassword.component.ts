import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient,HttpClientModule,HttpParams,HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.sass']
})
export class ForgotpasswordComponent implements OnInit {

	forgotpasswordFrom: FormGroup;
	returnUrl: string;
	loading = false;
	submitted = false;
  seesion_key_data="";
  token_data = "";
  uid_data= "";
  resonseError="";


  constructor(private formBuilder: FormBuilder,
    private http: HttpClient,
    private router: Router,private route: ActivatedRoute) {
   
   }

  ngOnInit() {

 console.log("register token"+this.route.snapshot.params.token);
    this.token_data = this.route.snapshot.params.token;
    this.uid_data = this.route.snapshot.params.uidb64;
    console.log("register token"+this.uid_data + "5555" +this.token_data);
   // this.activatedRoute.queryParams.subscribe(params => {
   //      let date = params['token'];
   //      console.log(date); // Print the parameter to the console. 
   //  });
   

    this.seesion_key_data = localStorage.getItem("key_session");  
         console.log("seesion_key_data"+this.seesion_key_data);
         if(this.seesion_key_data)
         {
          console.log("if condition search history");
          this.router.navigate(['home']);
         }

  	this.forgotpasswordFrom = this.formBuilder.group({
            exampleInputEmail1: ['',Validators.required],
            exampleInputPassword2: ['', Validators.required]
        });
  }

  get f() { return this.forgotpasswordFrom.controls; }

  onSubmit_forgotPassword() {
    
        this.submitted = true;
        // stop here if form is invalid
        if (this.forgotpasswordFrom.invalid) {
            return;
        }
        else if(this.f.exampleInputEmail1.value !== this.f.exampleInputPassword2.value)
        {
          return;
        }
        else
        {
         this.resonseError = "";

           const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json'
          })
          };


          var body = {
                      'new_password1' : this.f.exampleInputEmail1.value,
                      'new_password2': this.f.exampleInputPassword2.value,
                      'uid' : this.uid_data,
                      'token': this.token_data
                                      };


         this.http.post<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/password/reset/confirm/",body, httpOptions)
          .subscribe(data => 
          {
            console.log("forgot password"+JSON.stringify(data));
             this.router.navigate(['resetonpassword']);
            // var status_data= data.status;
            //  if(JSON.stringify(status_data) == "200")
            //   {
            //      this.router.navigate(['login']);
            //   }
          },
           error => 
           {
            console.log("Error>>" + JSON.stringify(error));
            console.log("Error>>" + JSON.stringify(error.status));
            if(JSON.stringify(error.status) == "400")
            {
              this.resonseError = JSON.stringify(error.statusText).replace(/\"/g, "");
              console.log("Error>>" + JSON.stringify(this.resonseError));
              window.alert("Plaese use the email link to change password.");
            }
          }
          );

        }

    }

}
