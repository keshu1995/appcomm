import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import  { HttpClient,HttpClientModule,HttpParams,HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.sass']
})
export class ResetpasswordComponent implements OnInit {

  seesion_key_data = "";

   constructor(private formBuilder: FormBuilder,private http: HttpClient,private router: Router,
) { }

  ngOnInit() {

    this.seesion_key_data = localStorage.getItem("key_session");  
         console.log("seesion_key_data"+this.seesion_key_data);
         if(this.seesion_key_data )
         {
          console.log("if condition search history");
          this.router.navigate(['home']);
         }
  }

  onSubmit_resetpass()
  {
  	this.router.navigate(['home']);
  }

}
