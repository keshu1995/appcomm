import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { HomeComponent } from './home/home.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ForgotpasswordreqComponent } from './forgotpasswordreq/forgotpasswordreq.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { ResetonpasswordComponent } from './resetonpassword/resetonpassword.component';
import { SearchhistoryComponent } from './searchhistory/searchhistory.component';
import { LogoutComponent } from './logout/logout.component';
import { ProfileComponent } from './profile/profile.component';
import { ConfirmregistrationComponent } from './confirmregistration/confirmregistration.component';
import { ResetpasswordfieldsComponent } from './resetpasswordfields/resetpasswordfields.component';
import { ExtendedsearchComponent } from './extendedsearch/extendedsearch.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { TermsofuseComponent } from './termsofuse/termsofuse.component';
import { PrivacylinkComponent } from './privacylink/privacylink.component';

const routes: Routes = [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
 	  { path: 'login', component: LoginComponent },
 	  { path: 'registration/confirm-email/:token', component: RegistrationComponent },
 	  { path: 'extendedsearch/:token', component: ExtendedsearchComponent },
 	  { path: 'extendedsearch/', component: ExtendedsearchComponent },
 	  { path: 'forgotpassword/reset/confirm/:uidb64/:token', component: ForgotpasswordComponent },
	  {
	    path: 'login',
	    component: LoginComponent,
	    data: { title: 'login' }
	  },
	  
	  {
	    path: 'registration',
	    component: RegistrationComponent,
	    data: { title: 'registration' }
	  },
	  { path: 'home', component: HomeComponent },
	  {
	    path: 'home',
	    component: HomeComponent,
	    data: { title: 'home' }
	  },
	  
	  {
	    path: 'forgotpassword',
	    component: ForgotpasswordComponent,
	    data: { title: 'forgotpassword' }
	  },
	  { path: 'forgotpasswordreq', component: ForgotpasswordreqComponent },
	  {
	    path: 'forgotpasswordreq',
	    component: ForgotpasswordreqComponent,
	    data: { title: 'forgotpasswordreq' }
	  },
	  { path: 'resetpassword', component: ResetpasswordComponent },
	  {
	    path: 'resetpassword',
	    component: ResetpasswordComponent,
	    data: { title: 'resetpassword' }
	  },
	  { path: 'resetonpassword', component: ResetonpasswordComponent },
	  {
	    path: 'resetonpassword',
	    component: ResetonpasswordComponent,
	    data: { title: 'resetonpassword' }
	  },
	  { path: 'searchhistory', component: SearchhistoryComponent },
	  {
	    path: 'searchhistory',
	    component: SearchhistoryComponent,
	    data: { title: 'searchhistory' }
	  },
	  { path: 'logout', component: LogoutComponent },
	  {
	    path: 'logout',
	    component: LogoutComponent,
	    data: { title: 'logout' }
	  },
	  { path: 'profile', component: ProfileComponent },
	  {
	    path: 'profile',
	    component: ProfileComponent,
	    data: { title: 'profile' }
	  },
	   { path: 'confirmregistration', component: ConfirmregistrationComponent },
	  {
	    path: 'confirmregistration',
	    component: ConfirmregistrationComponent,
	    data: { title: 'confirmregistration' }
	  },
	  { path: 'resetpasswordfields', component: ResetpasswordfieldsComponent },
	  {
	    path: 'resetpasswordfields',
	    component: ResetpasswordfieldsComponent,
	    data: { title: 'resetpasswordfields' }
	  },
	  { path: 'extendedsearch', component: ExtendedsearchComponent },
	  {
	    path: 'extendedsearch',
	    component: ExtendedsearchComponent,
	    data: { title: 'v' }
	  },
	  { path: 'thankyou', component: ThankyouComponent },
	  {
	    path: 'thankyou',
	    component: ThankyouComponent,
	    data: { title: 'y' }
	  },
	  { path: 'termsofuse', component: TermsofuseComponent },
	  {
	    path: 'termsofuse',
	    component: TermsofuseComponent,
	    data: { title: 'y' }
	  },
	  { path: 'privacylink', component: PrivacylinkComponent },
	  {
	    path: 'privacylink',
	    component: PrivacylinkComponent,
	    data: { title: 'y' }
	  },

	  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
