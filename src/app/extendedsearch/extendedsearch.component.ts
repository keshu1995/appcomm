import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Location } from '@angular/common';
import * as moment from 'moment';

@Component({
  selector: 'app-extendedsearch',
  templateUrl: './extendedsearch.component.html',
  styleUrls: ['./extendedsearch.component.sass']
})
export class ExtendedsearchComponent implements OnInit {

  seesion_key_data = "";
  extendedSearchform: FormGroup;
  returnUrl: string;
  loading = false;
  submitted = false;
  country = [];
  token_data = "";
  uid_data= "";
  resonseError="";
  search_data = "";
  searchText = "";
  searchNotes ="";
  loader=false;
  not_found = "";
  countrydiscovery ="";
  countryorigin= "";
  countryorigin_val="";
  countrydiscovery_val="";
  oldest_hit_datetime = "";
  countries_timezone:any;
  timezone_var:string;

 constructor(
  private formBuilder: FormBuilder,
  private http: HttpClient,
  private router: Router,
  private route: ActivatedRoute
  ) { 

    this.getJSON().subscribe(data => {
        this.country = data.countries;

    });

    this.getJSON_timezone().subscribe(data => {
        this.countries_timezone = data.countries;
         //console.log("111111111111111"+JSON.parse(JSON.stringify(data.countries[1].country_code)));

         var timezone_length = Object.keys(this.countries_timezone).length;
         var country_of_residence_local = localStorage.getItem("country_of_residence");
         //alert(country_of_residence_local);
         var i;

         for(i=0;i<timezone_length;i++){
                if((JSON.parse(JSON.stringify(this.countries_timezone[i].country_code)))== country_of_residence_local){
                  //console.log("timezones"+(JSON.parse(JSON.stringify(this.countries_timezone[i].timezones))));
                  this.timezone_var = JSON.stringify(this.countries_timezone[i].timezones[0]);
                  //alert(this.timezone_var);
                }

              }

      });
}

   public getJSON_timezone(): Observable<any> {
        return this.http.get("./assets/countries.json");
    }

   ngOnInit() {
    this.search_data = this.route.snapshot.params.token;
    this.seesion_key_data = localStorage.getItem("key_session");  
         if(this.seesion_key_data === "null" || this.seesion_key_data === "" || this.seesion_key_data === null)
           {
           this.router.navigate(['login']);
           }
    this.extendedSearchform = this.formBuilder.group({
            searchText: ['',Validators.required],

            searchNotes:"",
        });

    if(this.search_data){
      this.loader=true;
      this.search(this.search_data,this.seesion_key_data);
    }
  }


  public getJSON(): Observable<any> {
        return this.http.get("./assets/country.json");
    }

   
  get f() { return this.extendedSearchform.controls; }

  

      load() 
      {
        if(this.route.snapshot.params.token){
          this.router.navigate(['extendedsearch']);
        }else{
          location.reload();
        }         
       }

      onGoToThankyou()
      {
        this.router.navigate(['thankyou']);
      }  

   selectOptionCountryorigin(id: string) {
    console.log("test "+id);
      this.countryorigin = id;
      this.countryorigin_val="";
      this.countrydiscovery_val="";
    }

   selectOptionCountry(id: string) {
    console.log("countryorigin "+id)
     this.countrydiscovery = id;
     
     this.countrydiscovery_val="";
     this.countryorigin_val=""; 
    }

  onSubmit_search()
  {     
        this.submitted = true;
        // stop here if form is invalid      
      if (this.extendedSearchform.invalid){
        return;     
         }

      if(this.countryorigin =="" && this.countrydiscovery=="")
        
          { console.log("in 1");
            if(this.countryorigin =="")
              {
                console.log("in 2");
                this.countryorigin_val='1';
                this.countrydiscovery_val='1';
                this.not_found="3";
             return;
             }
             if(this.countrydiscovery==""){
               console.log("in 3");
              this.countrydiscovery_val='1';
              this.countryorigin_val='1';
              this.not_found="3";             
              return;
             }
              console.log("in 4");
        }
        else
        {
           console.log("else 1");
           this.loader=true;
           this.resonseError = "";
           this.seesion_key_data = localStorage.getItem("key_session");
           //var data = this.f.searchText.value;
           var datatrim = this.f.searchText.value.replace(/\s/g, "");
           var data = datatrim;
           console.log('data'+data+"test");
           this.search(data,this.seesion_key_data);           
          }
  
    } 

    search(data,seesion_key_data){
      console.log("submit "+this.countryorigin+" t "+ this.countrydiscovery);
      this.loader=true;
        const httpOptions = {
              headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'Authorization': 'Token '+seesion_key_data,
              })
            };
            // https://acc-lookup.sjerlok.com:8081/api/lookup/JTMRJREV90D179287/?object_country_of_origin=nl
            this.http.get<any>(`https://acc-lookup.sjerlok.com:8081/api/lookup/${data}?object_country_of_origin=${this.countryorigin}&object_country_found=${this.countrydiscovery}`,httpOptions)
                  .subscribe(data => 
                  {
                      console.log(" number_hit_searches "+JSON.stringify(data));
                      this.oldest_hit_datetime = this.formatSample(data.oldest_hit_datetime);
                      if(data.number_hit_searches == "0")
                      {
                       this.not_found = "1" ;
                      }

                     if(data.number_hit_searches != "0")
                      {
                       this.not_found = "2" ;
                      }
                      this.loader=false;             
                  },
                   error => 
                   {
                    console.log("error "+JSON.stringify(error));
                    this.loader=false;
                    if ((error.status) == 404) {
                       this.not_found= "404";
                    }            
              }
          );
    }

    formatSample(s){

      const moment = require('moment-timezone');
      var dteae = moment(s).tz(this.timezone_var.replace(/\"/g, "")).format('DD/MM/YYYY,h:mm:ss a');
      return dteae;
    }

    // formatSample(s){
    //   var ldf = moment.localeData().longDateFormat('l');
    //   // return moment.utc(s, [ldf, 'YY/MM/DD , h:mm:ss']).format('DD MMMM YYYY , h:mm:ss');
    //   // return moment.utc(s, [ldf, 'DD-MM-YY , h:mm:ss']).format('DD MMMM YYYY , h:mm:ss');
    //   return moment.utc(s, [ldf, 'DD-MM-YY , h:mm:ss']).format('DD/MM/YYYY,h:mm:ss');
    // }

    // convertUTCDateToLocalDate(date) {
    //   var newDate = new Date(new Date(date).getTime() - new Date(date).getTimezoneOffset()*60*1000);
    //   var return_data = this.formatSample(newDate);
    //   return return_data;   
    // }
    
}
