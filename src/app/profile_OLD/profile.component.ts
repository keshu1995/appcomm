import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AppComponent } from '../app.component';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {

	seesion_key_data = "";
  resonseError ="";
  submitted = false;
  profileForm: FormGroup;
  phonecode = [];
  code= "+31";
  country = "";
  country_error:string;
  resonseError_profile= "";

  email_val="";
  firstname_val = "";
  lastname_val = "";
  phone_val :any;
  countrydiscovery="";

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService,
    private appComponent:AppComponent,
    ) 
  {
    this.getJSON().subscribe(data => {
              console.log(data.countries);
              this.phonecode = data.countries;
        });

    this.profileForm = this.formBuilder.group({
              firstName: ['', Validators.required],
              lastName: ['', Validators.required],
              phone: ['' , Validators.required],
              email1: ['' , Validators.required],
          });
   }


  ngOnInit() {

    this.seesion_key_data = localStorage.getItem("key_session");  
         console.log("seesion_key_data"+this.seesion_key_data);

    this.load_user_data();
    
  	
         // if(this.seesion_key_data === "null" || this.seesion_key_data === "" || this.seesion_key_data === null)
         // {
         //  console.log("if condition search history");
         //  this.router.navigate(['login']);
         // }

    //form validation
    
  }

  public getJSON(): Observable<any> {
        return this.http.get("./assets/phonecode.json");
    }

  onGoToResetPassword()
  {
  	this.router.navigate(['resetpasswordfields']);
  	
  }

   //form submit
  onSubmit_profileForm()
  {   
    console.log("phone "+this.f.phone.value);
       this.resonseError = "";
       this.country_error= "";
       this.resonseError_profile="";
       this.submitted = true;
        // stop here if form is invalid
        if (this.profileForm.invalid) {
            return;
        }
        if(this.country==''){
          this.countrydiscovery="1";
          return;
        }
        else
        { 
         const httpOptions = {
           headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': 'Token '+this.seesion_key_data,
              })
          };

          var body = {
            'first_name':this.f.firstName.value,
            'last_name' : this.f.lastName.value,
            'phone_number' : this.code+this.f.phone.value
            };

         this.http.put<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/user/",body, httpOptions)
          .subscribe(data => 
          {
             this.toastr.success("Profile Updated");
             this.appComponent.update();
          },
           error => 
           {
              if(JSON.stringify(error.status) == "401")
              {
                this.resonseError_profile = JSON.stringify(error['error']['detail']).replace(/\"/g, "");
              }
              else if(JSON.stringify(error.status) == "400")
              {
                this.resonseError_profile = JSON.stringify(error['error']['phone_number'][0]).replace(/\"/g, "");
              }
           }
          );
        }
  }

  deleteaccount()
  {
    const httpOptions1 = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'Token '+this.seesion_key_data,
        })
      };

      this.http.get<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/user/", httpOptions1)
      .subscribe(data => 
      {
        console.log("sucees delete>>"+JSON.stringify(data));

         this.router.navigate(['home']);
        
      },
       error => 
       {
         console.log("error delte"+ JSON.stringify(error));
        if(JSON.stringify(error.status) == "401")
        {
          //var key = JSON.stringify(error.error.email["0"]).replace(/\"/g, "");
          this.resonseError = JSON.stringify(error.statusText).replace(/\"/g, "");
          //console.log("Error>>" + JSON.stringify(this.resonseError));
          window.alert(this.resonseError);
        }
      });
  }
   
  get f() { return this.profileForm.controls; }

  selectOption(id: string) {
    //getted from event
    this.code = id;

  }

  selectOptionCountry(id: string) {
    //getted from event
    this.country = id;
    console.log(this.country);
    this.countrydiscovery='';
  }

  //load data
  load_user_data()
  {
     const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'Authorization': 'Token '+this.seesion_key_data,
          })
          };

           this.http.get<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/user/", httpOptions)
          .subscribe(data => 
          {
           // this.router.navigate(['login']);
            console.log("sucees>>"+JSON.stringify(data));
            console.log("phone_val>> "+JSON.stringify(data.phone_number).replace(/\"/g, ""));
            this.firstname_val = JSON.stringify(data.first_name).replace(/\"/g, "");
            this.lastname_val = JSON.stringify(data.last_name).replace(/\"/g, "");
            this.phone_val = (JSON.stringify(data.phone_number).replace(/\"/g,"")).replace("+", "");
            this.email_val = JSON.stringify(data.email).replace(/\"/g, "");

          },
           error => 
           {
             console.log("Errorline>>" + JSON.stringify(error.error));
              if(JSON.stringify(error.status) == "401")
              {
                console.log("line 196 profile"+JSON.stringify(error['error']['detail']).replace(/\"/g, ""));
                this.resonseError_profile = JSON.stringify(error['error']['detail']).replace(/\"/g, "");
                window.alert(this.resonseError_profile);
              }
              
           }
          );
  }

}
