import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient,HttpClientModule,HttpParams,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.sass']
})
export class RegistrationComponent implements OnInit {

	registrationForm: FormGroup;
	returnUrl: string;
	loading = false;
	submitted = false;
  resonseError = "";
  seesion_key_data="";
  register_token = "";
  country = "";
  country_error:string;

  invitenote_val="";
  country_of_origin:any;
  countrydiscovery ="0";
  code_in = "+1";
  country_in ="";
  phone= "";
  countryName:any;
  company_val =1;
  countrycode=[];

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    ) {

       this.getJSON1().subscribe(data => {
        // console.log("data "+JSON.parse(JSON.stringify((data.countries)[1].code)));
        this.countrycode = data.countries;

        // console.log("length "+Object.keys(this.countrycode).length);
       });

     // const navigation = this.router.getCurrentNavigation().extras.name;
     // console.log(navigation);
   }



  ngOnInit() {

    console.log("register token "+this.route.snapshot.params.token);
    this.register_token = this.route.snapshot.params.token;

    if(this.register_token)
    {
          const httpOptions1 = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json'
          })
          };
          var body1 = {
            'key':this.register_token
           };

           var v = this.http.post<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/registration/verify-email/",body1, httpOptions1)
          .subscribe(data => 
          {
            console.log("sucees>>"+JSON.stringify(data));
            this.router.navigate(['login']);            
          },
           error => 
           {          
            if(JSON.stringify(error.status) == "404")
            {
              this.resonseError = JSON.stringify(error.statusText).replace(/\"/g, "");
              console.log("Error>>" + JSON.stringify(this.resonseError));
              window.alert(this.resonseError);
            }
          }
        );
    }  

     this.seesion_key_data = localStorage.getItem("key_session");  
         console.log("seesion_key_data"+this.seesion_key_data);
         if(this.seesion_key_data )
         {
          console.log("if condition search history");
          this.router.navigate(['home']);
         }

        this.registrationForm = this.formBuilder.group({
            inputEmail4: ['', Validators.required],
            inputPassword4: ['', Validators.required],
            inputAddress: ['', Validators.required],
            inputAddress2: ['',Validators.required],
            inputAddress3: ['', Validators.required],
            customCheck: ['', Validators.required],
            country:['', Validators.required],
            invitenote:"",
            company:['', Validators.required],
        });
  }

  get f() { return this.registrationForm.controls; }

  onSubmit_reg() {  
       // window.alert(this.f.company.value);
       // window.alert(this.f.invitenote.value);
        //window.alert(this.f.country); 
       console.log("checkbox value >> "+this.f.customCheck.value);
       this.resonseError = "";  
        this.submitted = true;

        if(this.country==''){
          this.countrydiscovery="1";
        }
        // stop here if form is invalid
        if (this.registrationForm.invalid) {
            return;
        }
        else if(this.f.inputAddress2.value != this.f.inputAddress3.value)
        {
          return;
        }
        else if(this.f.customCheck.value == "false" || this.f.customCheck.value == false)
        {
          return;
        }
        else
        {          
           console.log("checkbox value"+this.f.customCheck.value);
            const httpOptions = {
              headers: new HttpHeaders({
                'Content-Type':  'application/json'
              })
          };

          var body = {
            'email':this.f.inputAddress.value,
            'password1' : this.f.inputAddress2.value,
            'password2': this.f.inputAddress3.value,
            'first_name' : this.f.inputEmail4.value,
            'last_name': this.f.inputPassword4.value,
            'has_agreed_to_tos':'true',
            'country_of_residence':this.country,
            'invited_by':this.f.invitenote.value,
            'company_type':this.f.company.value
          };

         var v = this.http.post<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/registration/",body, httpOptions)
          .subscribe(data => 
          {
            console.log("sucees>>"+JSON.stringify(data));

            this.router.navigate(['confirmregistration']);           
          },
           error => 
           {
            console.log("Error>>" + JSON.stringify(error.status));
            if(JSON.stringify(error.status) == "400")
            {
              this.resonseError = JSON.stringify(error.error.email["0"]).replace(/\"/g, "");
            }
          }
          );
        }
    }

  selectOptionCountry(id: string) {
    //getted from event
    this.country = id;
    // console.log("selectOptionCountry "+(id));
    this.countrydiscovery='';
  }

  public getJSON1(): Observable<any> {
        return this.http.get("./assets/country.json");
  }




       

}
