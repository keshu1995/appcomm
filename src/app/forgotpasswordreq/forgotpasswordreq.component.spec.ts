import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgotpasswordreqComponent } from './forgotpasswordreq.component';

describe('ForgotpasswordreqComponent', () => {
  let component: ForgotpasswordreqComponent;
  let fixture: ComponentFixture<ForgotpasswordreqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForgotpasswordreqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgotpasswordreqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
