import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  simpleSearchForm: FormGroup;
  returnUrl: string;
  loading = false;
  submitted = false;
  sessionDetails = "";  
  seesion_key_data = "";

  constructor(private formBuilder: FormBuilder,private http: HttpClient,private router: Router,private route: ActivatedRoute) {

  this.sessionDetails = localStorage.getItem("key_session");  
  console.log("home line 22"+this.sessionDetails);
  }

  ngOnInit() {
  
    this.simpleSearchForm = this.formBuilder.group({
            formGroupExampleInput: ['', Validators.required]
        });
  }

  get f() { return this.simpleSearchForm.controls; }

  onSubmit_serach() {
        this.submitted = true;
        //this._router.navigate(["/registration/" + this.f.formGroupExampleInput.value]);
        console.log("1111>>"+localStorage.getItem("key_session"));
        // stop here if form is invalid
        if(localStorage.getItem("key_session") === "null" || localStorage.getItem("key_session") === "" || localStorage.getItem("key_session") === null)
        {
          console.log("else if home");
          this.router.navigate(['login']);
          // return;
        }
        else if (this.simpleSearchForm.invalid) {
            return;
        }
        else
        {
          this.router.navigate(['/extendedsearch/'+this.f.formGroupExampleInput.value]);
          
        }
     }

      onGoToLogin()
      {
         this.router.navigate(['login']);
      }

      onGoToSearchHistory()
      {
        this.router.navigate(['searchhistory']);
      }

      goToProfile()
      {
         this.router.navigate(['profile']);
      }
      onGoToExtendedsearch(){
        this.router.navigate(['extendedsearch']);
      }

      goToLogout()
      {
          this.seesion_key_data = localStorage.getItem("key_session");  
         console.log('Token'+this.seesion_key_data);
         const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'Authorization': 'Token '+this.seesion_key_data,
          })
          };

           this.http.post<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/logout/", httpOptions)
          .subscribe(data => 
          {
           // this.router.navigate(['login']);
            console.log("sucees>>"+JSON.stringify(data));

            if(JSON.stringify(data.detail))
            {
              localStorage.removeItem("key_session");
              this.router.navigate(['logout']);
            }

          },
           error => 
           {
            console.log("Errordfgdfgfd>>" + JSON.stringify(error));
           
           }
          );
      }

}
