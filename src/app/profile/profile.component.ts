import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AppComponent } from '../app.component';
import { ToastrService } from 'ngx-toastr';

import { PhoneNumberUtil, PhoneNumber } from 'google-libphonenumber';

const phoneNumberUtil = PhoneNumberUtil.getInstance();

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {

  seesion_key_data = "";
  resonseError ="";
  submitted = false;
  profileForm: FormGroup;
  phonecode = [];
  countrycode=[];
  code = "";
  country = "";
  country_error:string;
  resonseError_profile= "";

  email_val="";
  firstname_val = "";
  lastname_val = "";
  phone_val :any;
  invitenote_val="";
  country_of_origin:any;
  countrydiscovery ="0";
  code_in = "+1";
  country_in ="";
  phone= "";
  countryName:any;
  company_val =1;
  countries_timezone="";


  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService,
    private appComponent:AppComponent,
    ) 
  {
    this.getJSON().subscribe(data => {
        // console.log(data.countries);
        this.phonecode = data.countries;
      });

    this.getJSON1().subscribe(data => {
        // console.log("data "+JSON.parse(JSON.stringify((data.countries)[1].code)));
        this.countrycode = data.countries;

        // console.log("length "+Object.keys(this.countrycode).length);
    });

    

    this.profileForm = this.formBuilder.group({
              firstName: ['', Validators.required],
              lastName: ['', Validators.required],
              email1: ['' , Validators.required],
              code:"",
              country:"",
              invitenote:"",
              company:"",

          });
   }

   // PhoneNumberValidator(){
 
      // const phoneNumber = phoneNumberUtil.parseAndKeepRawInput('+93-755-5516-261', 'AF');
      // // validNumber = phoneNumberUtil.isValidNumber(phoneNumber);
      // console.log("validNumber "+phoneNumber.getCountryCode());

      // var numberProto = phoneNumberUtil.parse("+93-755-5516-261", "")
      // console.log("val" + numberProto.countryCode.toString());
  // }

  ngOnInit() {

    this.seesion_key_data = localStorage.getItem("key_session");  
         // console.log("seesion_key_data"+this.seesion_key_data);

    this.load_user_data();         
  }

  public getJSON(): Observable<any> {
        return this.http.get("./assets/phonecode.json");
    }

  public getJSON1(): Observable<any> {
        return this.http.get("./assets/country.json");
    }


  onGoToResetPassword()
  {
    this.router.navigate(['resetpasswordfields']);
  }

   //form submit
  onSubmit_profileForm()
  {   //window.alert(this.f.company.value);
       this.resonseError = "";
       this.country_error= "";
       this.resonseError_profile="";
       this.submitted = true;
        // stop here if form is invalid
        if (this.profileForm.invalid) {
            return;
        }
        if(this.country==''){
          this.countrydiscovery="1";
          return;
        }
        else
        { 
         const httpOptions = {
           headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': 'Token '+this.seesion_key_data,
              })
          };
          
          var body = {
            'first_name':this.f.firstName.value,
            'last_name' : this.f.lastName.value,
            'country_of_residence':this.country,
            'invited_by':this.f.invitenote.value,
            'company_type':this.f.company.value
         
            };

         this.http.put<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/user/",body, httpOptions)
          .subscribe(data => 
          {
              console.log("profile "+JSON.stringify(data))
             this.toastr.success("Profile Updated");
             this.appComponent.update();
          },
           error => 
           {
              if(JSON.stringify(error.status) == "401")
              {
                this.resonseError_profile = JSON.stringify(error['error']['detail']).replace(/\"/g, "");
              }
              else if(JSON.stringify(error.status) == "400")
              {
                 console.log("error "+JSON.stringify(error['error']));
                //this.resonseError_profile = JSON.stringify(error['error']['phone_number'][0]).replace(/\"/g, "");
              }
           }
          );
        }
  }

  deleteaccount()
  {
    const httpOptions1 = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'Token '+this.seesion_key_data,
        })
      };

      this.http.get<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/user/", httpOptions1)
      .subscribe(data => 
      {
        // console.log("sucees delete>>"+JSON.stringify(data));

         this.router.navigate(['home']);
        
      },
       error => 
       {
         // console.log("error delte"+ JSON.stringify(error));
        if(JSON.stringify(error.status) == "401")
        {
          //var key = JSON.stringify(error.error.email["0"]).replace(/\"/g, "");
          this.resonseError = JSON.stringify(error.statusText).replace(/\"/g, "");
          //console.log("Error>>" + JSON.stringify(this.resonseError));
          window.alert(this.resonseError);
        }
      });
  }
   
  get f() { return this.profileForm.controls; }

  selectOption(id: string) {
    // console.log("selectOption " + id)
    //getted from event
    this.code = id;

  }

  selectOptionCountry(id: string) {
    //getted from event
    this.country = id;
    // console.log("selectOptionCountry "+(id));
    this.countrydiscovery='';
  }

  //load data
  load_user_data()
  {
     const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'Authorization': 'Token '+this.seesion_key_data,
          })
          };

           this.http.get<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/user/", httpOptions)
          .subscribe(data => 
          {
           // this.router.navigate(['login']);
             //console.log("sucees>>"+JSON.stringify(data));
           // console.log("JSON.stringify(data.company_type)>> "+data.company_type);
            this.firstname_val = JSON.stringify(data.first_name).replace(/\"/g, "");
            this.lastname_val = JSON.stringify(data.last_name).replace(/\"/g, "");
            this.company_val = (data.company_type)
            //this.phone_val = JSON.stringify(data.phone_number).replace(/\"/g,"");
            this.email_val = JSON.stringify(data.email).replace(/\"/g, "");
            this.country_of_origin = JSON.parse(JSON.stringify(data.country_of_residence));
            this.invitenote_val = JSON.parse(JSON.stringify(data.invited_by));
             
            // const phoneNumber = phoneNumberUtil.parseAndKeepRawInput(this.phone_val, this.country_of_origin);
              // validNumber = phoneNumberUtil.isValidNumber(phoneNumber);
              // console.log("validNumber "+phoneNumber.getCountryCode());
              // console.log("numberProto "+phoneNumber.getNationalNumber());
              // this.phone_val=phoneNumber.getNationalNumber();
              // this.code_in="+"+phoneNumber.getCountryCode();
              // this.code=this.code_in;
              var i=0;
              var length=Object.keys(this.countrycode).length;

              for(i=0;i<length;i++){
                if((JSON.parse(JSON.stringify(this.countrycode[i].code)))==this.country_of_origin){
                  // console.log("name "+JSON.parse(JSON.stringify(this.countrycode[i].name)))
                  this.country_in=this.country_of_origin;
                  this.country=this.country_in;
                }
              }
           
              // var numberProto = phoneNumberUtil.parse("+93-755-5516-261", "")
      /*   console.log("sucess data is come in correct formate"+ this.code_in ,this.phone_val, this.country_of_origin);*/
          },
           error => 
           {
             // console.log("Errorline>>" + JSON.stringify(error.error));
              if(JSON.stringify(error.status) == "401")
              {
                // console.log("line 196 profile"+JSON.stringify(error['error']['detail']).replace(/\"/g, ""));
                this.resonseError_profile = JSON.stringify(error['error']['detail']).replace(/\"/g, "");
                window.alert(this.resonseError_profile);
              }
           }
          );
  }

}
