import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';




@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {

	seesion_key_data = "";
  resonseError ="";
  submitted = false;
  profileForm: FormGroup;
  phonecode = [];
  code= "+31";
  country = "Netherlands";
  country_error:string;
  resonseError_profile= "";

  firstname_val = "";
  lastname_val = "";
  phone_val = "";

  constructor(private formBuilder: FormBuilder,private http: HttpClient,private router: Router,) {
  this.getJSON().subscribe(data => {
            console.log(data.countries);
            this.phonecode = data.countries;
        });
         }


  ngOnInit() {

    this.seesion_key_data = localStorage.getItem("key_session");  
         console.log("seesion_key_data"+this.seesion_key_data);

    this.load_user_data();

  	
         // if(this.seesion_key_data === "null" || this.seesion_key_data === "" || this.seesion_key_data === null)
         // {
         //  console.log("if condition search history");
         //  this.router.navigate(['login']);
         // }

    //form validation
    this.profileForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            phone: ['', Validators.required],
        });
  }

  public getJSON(): Observable<any> {
        return this.http.get("./assets/phonecode.json");
    }

  onGoToResetPassword()
  {
  	this.router.navigate(['resetpasswordfields']);
  	
  }

  deleteaccount()
  {
    const httpOptions1 = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'Authorization': 'Token '+this.seesion_key_data,
          })
          };


          var body1 = {
            'key':this.seesion_key_data
                      };


           var v = this.http.get<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/user/", httpOptions1)
          .subscribe(data => 
          {
            console.log("sucees delete>>"+JSON.stringify(data));

             this.router.navigate(['home']);
            
          },
           error => 
           {
             console.log("error delte"+ JSON.stringify(error));
            if(JSON.stringify(error.status) == "401")
            {
              //var key = JSON.stringify(error.error.email["0"]).replace(/\"/g, "");
              this.resonseError = JSON.stringify(error.statusText).replace(/\"/g, "");
              //console.log("Error>>" + JSON.stringify(this.resonseError));
              window.alert(this.resonseError);
            }
          }
          );
  }
   
  get f() { return this.profileForm.controls; }

  selectOption(id: string) {
    //getted from event
    this.code = id;
    console.log(this.code);
  }

  selectOptionCountry(id: string) {
    //getted from event
    this.country = id;
    console.log(this.country);
  }

  //form submit
  onSubmit_profileForm()
  {
    
       this.resonseError = "";
       this.country_error= "";
       this.resonseError_profile="";
      
       
        this.submitted = true;
        // stop here if form is invalid
        if (this.profileForm.invalid) {
            return;
        }
        else
        {
           console.log("firstname", this.f.firstName.value);
       console.log("lastname", this.f.lastName.value);
       console.log("countrycode", this.code);
        console.log("country", this.country);
        console.log("phoneno", this.code+this.f.phone.value);

             const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'Authorization': 'Token '+this.seesion_key_data,
          })
          };


          var body = {
            'first_name':this.f.firstName.value,
            'last_name' : this.f.lastName.value,
            'phone_number' : this.code+this.f.phone.value
                    };


           this.http.put<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/user/",body, httpOptions)
          .subscribe(data => 
          {
           // this.router.navigate(['login']);
            console.log("sucees>>"+JSON.stringify(data));


          },
           error => 
           {
             console.log("Error>>" + JSON.stringify(error));
              if(JSON.stringify(error.status) == "401")
              {
                console.log("line 163 profile"+JSON.stringify(error['error']['detail']).replace(/\"/g, ""));
                this.resonseError_profile = JSON.stringify(error['error']['detail']).replace(/\"/g, "");
              }
              else if(JSON.stringify(error.status) == "400")
              {
                console.log("line 177 profile"+JSON.stringify(error['error']['phone_number'][0]).replace(/\"/g, ""));
                this.resonseError_profile = JSON.stringify(error['error']['phone_number'][0]).replace(/\"/g, "");
              }
           }
          );
        }
  }


  //load data
  load_user_data()
  {
     const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'Authorization': 'Token '+this.seesion_key_data,
          })
          };

           this.http.get<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/user/", httpOptions)
          .subscribe(data => 
          {
           // this.router.navigate(['login']);
            console.log("sucees>>"+JSON.stringify(data));
            console.log("sucees>>"+JSON.stringify(data.first_name).replace(/\"/g, ""));
            this.firstname_val = JSON.stringify(data.first_name).replace(/\"/g, "");
            this.lastname_val = JSON.stringify(data.last_name).replace(/\"/g, "");
            this.phone_val = JSON.stringify(data.phone_number).replace(/\"/g, "");
            //this.email = JSON.stringify(data.email).replace(/\"/g, "");


          },
           error => 
           {
             console.log("Errorline>>" + JSON.stringify(error.error));
              if(JSON.stringify(error.status) == "401")
              {
                console.log("line 196 profile"+JSON.stringify(error['error']['detail']).replace(/\"/g, ""));
                this.resonseError_profile = JSON.stringify(error['error']['detail']).replace(/\"/g, "");
                window.alert(this.resonseError_profile);
              }
              
           }
          );
  }

}
