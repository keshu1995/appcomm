import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ForgotpasswordreqComponent } from './forgotpasswordreq/forgotpasswordreq.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { ResetonpasswordComponent } from './resetonpassword/resetonpassword.component';
import { SearchhistoryComponent } from './searchhistory/searchhistory.component';
import { LogoutComponent } from './logout/logout.component';
import { ProfileComponent } from './profile/profile.component';
import { ConfirmregistrationComponent } from './confirmregistration/confirmregistration.component';
import { ResetpasswordfieldsComponent } from './resetpasswordfields/resetpasswordfields.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { ExtendedsearchComponent } from './extendedsearch/extendedsearch.component';
import { ToastrModule } from 'ngx-toastr';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TermsofuseComponent } from './termsofuse/termsofuse.component';
import { PrivacylinkComponent } from './privacylink/privacylink.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    HomeComponent,
    ForgotpasswordComponent,
    ForgotpasswordreqComponent,
    ResetpasswordComponent,
    ResetonpasswordComponent,
    SearchhistoryComponent,
    LogoutComponent,
    ProfileComponent,
    ConfirmregistrationComponent,
    ResetpasswordfieldsComponent,
    ExtendedsearchComponent,
    ThankyouComponent,
    TermsofuseComponent,
    PrivacylinkComponent
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule,
    ToastrModule.forRoot()
      
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
