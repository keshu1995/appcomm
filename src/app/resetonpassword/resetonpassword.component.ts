import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-resetonpassword',
  templateUrl: './resetonpassword.component.html',
  styleUrls: ['./resetonpassword.component.sass']
})
export class ResetonpasswordComponent implements OnInit {

  seesion_key_data = "";

  constructor(private router: Router) { }

  ngOnInit() {

    this.seesion_key_data = localStorage.getItem("key_session");  
         console.log("seesion_key_data"+this.seesion_key_data);
         if(this.seesion_key_data )
         {
          console.log("if condition search history");
          this.router.navigate(['home']);
         }
  
  }

  onSubmit_resetonpass()
  {
  	this.router.navigate(['login']);
  }

}
