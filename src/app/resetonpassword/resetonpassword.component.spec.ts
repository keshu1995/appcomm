import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetonpasswordComponent } from './resetonpassword.component';

describe('ResetonpasswordComponent', () => {
  let component: ResetonpasswordComponent;
  let fixture: ComponentFixture<ResetonpasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetonpasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetonpasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
