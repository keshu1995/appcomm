import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import  { HttpClient,HttpClientModule,HttpParams,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';
// import moment from 'moment'

@Component({
  selector: 'app-searchhistory',
  templateUrl: './searchhistory.component.html',
  styleUrls: ['./searchhistory.component.sass']
})
export class SearchhistoryComponent implements OnInit {

  loading = false;
  submitted = false;
  resonseError_login: string;
  seesion_key_data = "";
  no_data_error ="";
  data_values = [];
  search_length:string; 
  public history_data = [];
  pager: any = {};
  error_data: boolean = true; 
  pagedItems: any[];
  countrycode=[];
  compare_countrycode = [];
  compare_countryname = [];
  countries_timezone:any;
  timezone_var:string;

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private router: Router,
) { 
     this.getJSON().subscribe(data => {
        console.log("data "+JSON.parse(JSON.stringify((data.countries)[1].code)));
        this.countrycode = data.countries;

          var length1=Object.keys(this.countrycode).length;

          for(var i=0;i<length1;i++){  
            this.compare_countrycode.push(this.countrycode[i].code);
            this.compare_countryname.push(this.countrycode[i].name);
          }  
          // console.log(`--------------------->>>>>>>>>>>>>>>   ${JSON.stringify(this.compare_countrycode[1])}   ----   ${this.compare_countryname[1]}`)


        // console.log("length "+Object.keys(this.countrycode).length);
    });


     this.getJSON_timezone().subscribe(data => {
        this.countries_timezone = data.countries;
         //console.log("111111111111111"+JSON.parse(JSON.stringify(data.countries[1].country_code)));

         var timezone_length = Object.keys(this.countries_timezone).length;
         var country_of_residence_local = localStorage.getItem("country_of_residence");
         //alert(country_of_residence_local);
         var i;

         for(i=0;i<timezone_length;i++){
                if((JSON.parse(JSON.stringify(this.countries_timezone[i].country_code)))== country_of_residence_local){
                  //console.log("timezones"+(JSON.parse(JSON.stringify(this.countries_timezone[i].timezones))));
                  this.timezone_var = JSON.stringify(this.countries_timezone[i].timezones[0]);
                  //alert(this.timezone_var);
                }
                // else
                // {
                //   alert("else");
                // }

              }

      });
}
    public getJSON(): Observable<any> {
        return this.http.get("./assets/country.json");
    }

    public getJSON_timezone(): Observable<any> {
        return this.http.get("./assets/countries.json");
    }

  ngOnInit() {



    // var moment = require('moment-timezone');
    // var qqq = moment().tz("Asia/Calcutta").format();

   
    // const date = moment('2019-10-01T11:11:18.491030Z');
     var country_of_residence_local = localStorage.getItem("country_of_residence");
    // //alert(country_of_residence_local);
     moment.locale(country_of_residence_local);
    // moment.localeData(country_of_residence_local).longDateFormat('L')
    // const date = this.formatSample();
     //console.log("line 59    "+date); 
         this.seesion_key_data = localStorage.getItem("key_session");  
         //console.log("seesion_key_data"+this.seesion_key_data);
         if(this.seesion_key_data === "null" || this.seesion_key_data === "" || this.seesion_key_data === null)
         {
          //console.log("if condition search history");
          this.router.navigate(['login']);
         }
         else
         {
            //console.log('Token'+this.seesion_key_data);
             const httpOptions = {
              headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'Authorization': 'Token '+this.seesion_key_data,
              })
              };

           this.http.get<any>("https://acc-lookup.sjerlok.com:8081/api/lookup/search-history/", httpOptions)
          .subscribe(data => 
          {
            //-------------------------------------

               //Create dummy data
               this.history_data = data;
               this.setPage(1);

            //---------------------------------------- 
            this.data_values = data;
            this.search_length = JSON.stringify(data.length);
          
          },
           error => 
           {

            this.error_data= false;
            //console.log("Error>>" + JSON.stringify(error));
             //console.log("Error>>" + JSON.stringify(error.status));
             //console.log("Error22>>" + JSON.stringify(error.non_field_errors[0]));

              if(JSON.stringify(error.status) == "400")
              {
                //console.log("<<<<<<<<<"+JSON.stringify(error.error));
                //this.resonseError_login = JSON.stringify(error.error.non_field_errors["0"]).replace(/\"/g, "");
                //console.log(this.resonseError_login);
                window.alert(JSON.stringify(error));
              }
           }
          );
         }
         

  }

    setPage(page: number) {
        // get pager object from service
        this.pager = this.getPager(this.history_data.length, page);
        // get current page of items
        this.pagedItems = this.history_data.slice(this.pager.startIndex, this.pager.endIndex + 1);
        // console.log("111111111>>>"+((this.pagedItems[49]).object_country_of_origin));
          var length=Object.keys(this.pagedItems).length;

  for(var i=0;i<length;i++){ 
   console.log("->>>>> "+this.compare_countryname[this.compare_countrycode.indexOf(this.pagedItems[i].object_country_of_origin)] +" ---- "+ JSON.stringify(this.pagedItems[i]))
   if(this.compare_countryname[this.compare_countrycode.indexOf(this.pagedItems[i].object_country_of_origin)]){
    console.log("cocvndsfjcdskl");

    (this.pagedItems[i]).object_country_of_origin=this.compare_countryname[this.compare_countrycode.indexOf(this.pagedItems[i].object_country_of_origin)];
        // this.pagedItems=this.pagedItems;
        console.log("tets "+(this.pagedItems[i]).object_country_of_origin);
   }
    if(this.compare_countryname[this.compare_countrycode.indexOf(this.pagedItems[i].object_country_found)]){
    console.log("cocvndsfjcdskl");

    (this.pagedItems[i]).object_country_found=this.compare_countryname[this.compare_countrycode.indexOf(this.pagedItems[i].object_country_found)];
        // this.pagedItems=this.pagedItems;
        console.log("tets "+(this.pagedItems[i]).object_country_found);
   }

  }
}

    

   onGoToHome()
   {
        this.router.navigate(['home']);
   }


   getPager(totalItems: number, currentPage: number = 1, pageSize: number = 50) {
        // calculate total pages
        let totalPages = Math.ceil(totalItems / pageSize);

        // ensure current page isn't out of range
        if (currentPage < 1) { 
            currentPage = 1; 
        } else if (currentPage > totalPages) { 
            currentPage = totalPages; 
        }
        
        let startPage: number, endPage: number;
        if (totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }

        // calculate start and end item indexes
        let startIndex = (currentPage - 1) * pageSize;
        let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

        // create an array of pages to ng-repeat in the pager control
        let pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    }


   formatSample(s){

    const moment = require('moment-timezone');
     //var d = new Date();
     //alert(d.getTimezoneOffset());
     //var offset = d.getTimezoneOffset();
     //alert(offset);
     //var country_of_residence_local = localStorage.getItem("country_of_residence");
     //var ldf = moment.localeData('nl').longDateFormat('l');
      // return moment.utc(s, [ldf, 'YY/MM/DD , h:mm:ss']).format('DD MMMM YYYY , h:mm:ss');
      // return moment.utc(s, [ldf, 'DD-MM-YY , h:mm:ss']).format('DD MMMM YYYY , h:mm:ss');
      //var format = zoneFormats[new Date().getTimezoneOffset()];
      //return moment.utc(s, [ldf, 'DD-MM-YY , h:mm:ss']).format('DD/MM/YYYY , h:mm:ss');
      //return moment(ldf).utcOffset(offset).format('DD/MM/YYYY , h:mm:ss');
      //return moment.utc(s, [ldf, 'DD-MM-YY , h:mm:ss']).format('DD/MM/YYYY,h:mm:ss a');

      // var testDateUtc = moment.utc(s);
      // var localDate = moment(testDateUtc).local('nl');
      // return localDate;

      //alert(this.timezone_var.replace(/\"/g, ""));

      var dteae = moment(s).tz(this.timezone_var.replace(/\"/g, "")).format('DD/MM/YYYY,h:mm:ss a');
      return dteae;
    }

    convertUTCDateToLocalDate(date) {
      // var moment = require('moment-timezone');
      // moment().tz(date, "Europe/Amsterdam").format();

      var newDate = new Date(new Date(date).getTime() - new Date(date).getTimezoneOffset()*60*1000);
      var return_data = this.formatSample(newDate);
      return return_data;   
    }

   





}
