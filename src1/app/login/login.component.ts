import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import  { HttpClient,HttpClientModule,HttpParams,HttpHeaders } from '@angular/common/http';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

	loginForm: FormGroup;
	returnUrl: string;
  loading = false;
  submitted = false;
  resonseError_login: string;
  sessionDetails="";
  seesion_key_data="";

  constructor(public AppComponent2:AppComponent, private formBuilder: FormBuilder,private http: HttpClient,private router: Router,
) { 

  }

  ngOnInit() {
   this.sessionDetails = localStorage.getItem("key_session");  
       var email_session = localStorage.getItem("key_session");
       console.log("logged in"+email_session);

       this.AppComponent2.user_log(email_session);

         this.seesion_key_data = localStorage.getItem("key_session") == null ? "" : localStorage.getItem("key_session");






        if(this.seesion_key_data )
         {
          console.log("if condition search history");
          this.router.navigate(['home']);
         }

       if(email_session)
       {
        //this.router.navigate(['registration']);
         console.log("logged in"+email_session);
       }


        this.loginForm = this.formBuilder.group({
            exampleInputEmail1: ['', Validators.required],
            exampleInputPassword1: ['', Validators.required]
        });
  }

   get f() { return this.loginForm.controls; }


   onSubmit() {

        this.submitted = true;
        this.resonseError_login= "";

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        else
        {
         const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json',
          })
          };


          var body = {
            'email':this.f.exampleInputEmail1.value,
            'password' : this.f.exampleInputPassword1.value
                    };


           this.http.post<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/login/",body, httpOptions)
          .subscribe(data => 
          {
           // this.router.navigate(['login']);
            console.log("succees>>"+JSON.stringify(data.key));

            if(JSON.stringify(data.key))
            {
              var key = JSON.stringify(data.key).replace(/\"/g, "");
               // console.log("sucees>>"+JSON.stringify(data.body.key));
               // console.log(">>>>"+JSON.stringify(data.body.key).replace(/\"/g, ""));
               this.AppComponent2.user_log(key);
               localStorage.setItem("key_session", key);
               console.log("suceegghghghghghs>>"+key);
               this.AppComponent2.update();
               this.router.navigate(['home']);

            }
          },
           error => 
           {
            console.log("Error>>" + JSON.stringify(error));
             // console.log("Error>>" + JSON.stringify(error.error['password'][0]));

              if(JSON.stringify(error.status) == "400")
              {
                if(JSON.stringify(error.error['password']))
                {
                  console.log("if if line 113");
                 this.resonseError_login = JSON.stringify(error.error['password']["0"]).replace(/\"/g, "");
                }
                else
                {
                  console.log("if if line 118");
                  this.resonseError_login = JSON.stringify(error.error.non_field_errors["0"]).replace(/\"/g, "");
                }
                
              }
           }
          );

        }

  }

  onGoToForgotPassReq()
  {
    this.router.navigate(['forgotpasswordreq']);
  }

  onGoToRegister()
  {
     this.router.navigate(['registration']);
  }

  
}
