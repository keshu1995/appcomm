import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetpasswordfieldsComponent } from './resetpasswordfields.component';

describe('ResetpasswordfieldsComponent', () => {
  let component: ResetpasswordfieldsComponent;
  let fixture: ComponentFixture<ResetpasswordfieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetpasswordfieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetpasswordfieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
