import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

import { AppComponent } from '../app.component';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-resetpasswordfields',
  templateUrl: './resetpasswordfields.component.html',
  styleUrls: ['./resetpasswordfields.component.sass']
})
export class ResetpasswordfieldsComponent implements OnInit {

	forgotpasswordfieldFrom: FormGroup;
	returnUrl: string;
	loading = false;
	submitted = false;
	seesion_key_data = "";
  resonseError = "";
  timeLeft: number = 60;
  interval;
  popup = false;

  constructor(public AppComponent2:AppComponent, private formBuilder: FormBuilder,private http: HttpClient,private router: Router,private toastr: ToastrService) { }

  ngOnInit() {
    this.seesion_key_data = localStorage.getItem("key_session"); 

    console.log("line 25 reset pass fiels"+this.seesion_key_data);

     // if(this.seesion_key_data === "null" || this.seesion_key_data === "" || this.seesion_key_data === null)
     //     {
     //      console.log("if condition search history");
     //      this.router.navigate(['login']);
     //     }

  	this.forgotpasswordfieldFrom = this.formBuilder.group({
            currentpassword: ['',Validators.required],
            newPassword: ['',[Validators.required, Validators.minLength(8)]],
            confirmPassword: ['',Validators.required]
        });
  }

  get f() { return this.forgotpasswordfieldFrom.controls; }

  startTimer() {
    this.interval = setInterval(() => {
     this.popup = true;
    },10)
    this.popup = false;
  }


  onSubmit_forgotPasswordField() {
    // this.toastr.success("Your new password is saved");
        this.submitted = true;
        this.resonseError = "";
        // stop here if form is invalid
        if (this.forgotpasswordfieldFrom.invalid) {
            return;
        }
        else if(this.f.newPassword.value !== this.f.confirmPassword.value)
        {
          return;
        }
        else
        {
          this.seesion_key_data = localStorage.getItem("key_session"); 
        	console.log("Token "+this.seesion_key_data);
        	const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'Authorization': 'Token '+this.seesion_key_data,
           })
          };

          var body = {
                'old_password' : this.f.currentpassword.value,
                'new_password1': this.f.newPassword.value,
                'new_password2' : this.f.confirmPassword.value
            };

         this.http.post<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/password/change/",body, httpOptions)
          .subscribe(data => 
          {
          	console.log("success>>"+JSON.stringify(data));
          	console.log("success>>"+JSON.stringify(data.detail));
            console.log("success>>"+JSON.stringify(data['detail']));
             this.toastr.success("Your new password is saved");
             this.router.navigate(['profile']);  
          },
           error => 
           {
            console.log("line 86 reset pass field"+JSON.stringify(error));            
            if(JSON.stringify(error.status) == "400")
            {
              //var key = JSON.stringify(error.error.email["0"]).replace(/\"/g, "");
              this.resonseError = JSON.stringify(error.statusText).replace(/\"/g, "");
            }
          }
          );
        }

    }


}
