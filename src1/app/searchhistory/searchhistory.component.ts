import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import  { HttpClient,HttpClientModule,HttpParams,HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-searchhistory',
  templateUrl: './searchhistory.component.html',
  styleUrls: ['./searchhistory.component.sass']
})
export class SearchhistoryComponent implements OnInit {

  loading = false;
  submitted = false;
  resonseError_login: string;
  seesion_key_data = "";
  no_data_error ="";
  data_values = [];
  search_length:string; 
  public history_data = [];
  // p: Number = 1;
  // count: Number = 5;
  pager: any = {};
  error_data: boolean = true;
   tab : any = 'tab1';
  tab1 : any
  tab2 : any
  Clicked : boolean


    // paged items
    pagedItems: any[];

  constructor(private formBuilder: FormBuilder,private http: HttpClient,private router: Router,
) { 

  //  //Create dummy data
  //   for (var i = 0; i < this.collection.count; i++) {
  //     this.collection.data.push(
  //       {
  //         id: i + 1,
  //         value: "items number " + (i + 1)
  //       }
  //     );

  //     this.config = {
  //     itemsPerPage: 5,
  //     currentPage: 1,
  //     totalItems: this.collection.count
  //   };
  // }
}

  // pageChanged(event){
  //   this.config.currentPage = event;
  // }

  ngOnInit() {

    console.log("line 32"+this.router.url);
         
         this.seesion_key_data = localStorage.getItem("key_session");  
         console.log("seesion_key_data"+this.seesion_key_data);
         if(this.seesion_key_data === "null" || this.seesion_key_data === "" || this.seesion_key_data === null)
         {
          console.log("if condition search history");
          this.router.navigate(['login']);
         }
         else
         {
            console.log('Token'+this.seesion_key_data);
             const httpOptions = {
              headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'Authorization': 'Token '+this.seesion_key_data,
              })
              };


           this.http.get<any>("https://acc-lookup.sjerlok.com:8081/api/lookup/search-history/", httpOptions)
          .subscribe(data => 
          {

            console.log("line 80 search history"+data);

            //-------------------------------------

               //Create dummy data
               this.history_data = data;
               this.setPage(1);

            //---------------------------------------- 
            this.data_values = data;
            this.search_length = JSON.stringify(data.length);
          
          },
           error => 
           {

            this.error_data= false;
            console.log("Error>>" + JSON.stringify(error));
             console.log("Error>>" + JSON.stringify(error.status));
             //console.log("Error22>>" + JSON.stringify(error.non_field_errors[0]));

              if(JSON.stringify(error.status) == "400")
              {
                //console.log("<<<<<<<<<"+JSON.stringify(error.error));
                //this.resonseError_login = JSON.stringify(error.error.non_field_errors["0"]).replace(/\"/g, "");
                //console.log(this.resonseError_login);
                window.alert(JSON.stringify(error));
              }
           }
          );
         }
         

  }

  onClick(check){
    //    console.log(check);
        if(check==1)
        {
          this.tab = 'tab1';
        }
        else{
          this.tab = 'tab2';
        }        
    }

   onSwitch(check)
   {
     switch (check) {
      case 1:
        return 'tab1';
      case 2:
        return 'tab2';
     
    }

}

    setPage(page: number) {
        // get pager object from service
        this.pager = this.getPager(this.history_data.length, page);
        console.log("111111111>>>"+JSON.stringify(this.pager));
        // get current page of items
        this.pagedItems = this.history_data.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

   onGoToHome()
   {
        this.router.navigate(['home']);
   }


   getPager(totalItems: number, currentPage: number = 1, pageSize: number = 50) {
        // calculate total pages
        let totalPages = Math.ceil(totalItems / pageSize);

        // ensure current page isn't out of range
        if (currentPage < 1) { 
            currentPage = 1; 
        } else if (currentPage > totalPages) { 
            currentPage = totalPages; 
        }
        
        let startPage: number, endPage: number;
        if (totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }

        // calculate start and end item indexes
        let startIndex = (currentPage - 1) * pageSize;
        let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

        // create an array of pages to ng-repeat in the pager control
        let pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    }

}
