import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtendedsearchComponent } from './extendedsearch.component';

describe('ExtendedsearchComponent', () => {
  let component: ExtendedsearchComponent;
  let fixture: ComponentFixture<ExtendedsearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtendedsearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendedsearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
