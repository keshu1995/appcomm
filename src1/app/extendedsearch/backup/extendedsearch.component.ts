import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-extendedsearch',
  templateUrl: './extendedsearch.component.html',
  styleUrls: ['./extendedsearch.component.sass']
})
export class ExtendedsearchComponent implements OnInit {

  seesion_key_data = "";
  extendedSearchform: FormGroup;
  returnUrl: string;
  loading = false;
  submitted = false;
  phonecode = [];
  token_data = "";
  uid_data= "";
  resonseError="";
  search_data = "";
  searchText = "";
  searchNotes ="";

  not_found = "";

 constructor(private formBuilder: FormBuilder,private http: HttpClient,private router: Router,private route: ActivatedRoute) { 
  this.getJSON().subscribe(data => {
            console.log(data.countries);
            this.phonecode = data.countries;
        });
}

  ngOnInit() {

    this.search_data = this.route.snapshot.params.token;

  	this.seesion_key_data = localStorage.getItem("key_session");  
         console.log("seesion_key_data"+this.seesion_key_data);
         if(this.seesion_key_data === "null" || this.seesion_key_data === "" || this.seesion_key_data === null)
         {
          console.log("if condition search history");
          this.router.navigate(['login']);
         }

    this.extendedSearchform = this.formBuilder.group({
            searchText: ['',Validators.required],
        });
  }


  public getJSON(): Observable<any> {
        return this.http.get("./assets/phonecode.json");
    }

  get f() { return this.extendedSearchform.controls; }

  onSubmit_search()
  {     
        this.submitted = true;
        // stop here if form is invalid
        if (this.extendedSearchform.invalid) {
            return;
        }

        else
        {
        
        	 this.resonseError = "";
	         this.seesion_key_data = localStorage.getItem("key_session");  
	         //console.log("https://acc-lookup.sjerlok.com:8081/api/lookup/"+this.f.formGroupExampleInput.value);
	         const httpOptions = {
	          headers: new HttpHeaders({
	            'Content-Type':  'application/json',
	            'Authorization': 'Token '+this.seesion_key_data,
	          })
	          };
      
            // if(this.search_data)
            // {
            //   var body = {
            //   'lookup_key':this.search_data
            //           };
            // }
            // else
            // {
              var body = {
                  'lookup_key': this.f.searchText.value
                      };
            //}

	          

	           this.http.get<any>("https://acc-lookup.sjerlok.com:8081/api/lookup/"+this.f.searchText.value,httpOptions)
	          .subscribe(data => 
	          {
              console.log("sucees>>"+JSON.stringify(data.status));
              
	           // this.router.navigate(['login']);
	            console.log("sucees>>"+JSON.stringify(data));
	          },
	           error => 
	           {
               if(JSON.stringify(error.status) == "")
              {
                console.log("Error>>" + JSON.stringify(error));
              console.log("sucees>>"+JSON.stringify(error.status));
                this.not_found = '';
              }
               
              // if(JSON.stringify(error.status) == 404)
              // {
              //   console.log("Error>>" + JSON.stringify(error));
              // console.log("sucees>>"+JSON.stringify(error.status));
              //   this.not_found = "404";
              // }

              // if(JSON.stringify(error.status) == 503)
              // {
              //   console.log("Error>>" + JSON.stringify(error));
              // console.log("sucees>>"+JSON.stringify(error.status));
              //   this.not_found = "503";
              // }

              // if(JSON.stringify(status) == 200)
              // {
              //   console.log("Error>>" + JSON.stringify(error));
              // console.log("sucees>>"+JSON.stringify(error.status));
              //   this.not_found = "200";
              // }



              

              
	           
	           }
	          );
        }
    }

}
