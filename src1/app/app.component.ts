import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent  implements OnInit{
  title = 'Appcomm';
  simpleSearchForm: FormGroup;
  returnUrl: string;
  loading = false;
  submitted = false;
  sessionDetails = "";  
  seesion_key_data = "";
  firstname_val="";
  lastname_val="";
  fname:any;
  lname:any;

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService
    ) {
    this.seesion_key_data = localStorage.getItem("key_session");
  }

    ngOnInit() {
      this.load_user_data();
    }

  user_log(data)
  {
    this.seesion_key_data = data;
  }

  goToLogout()
      {
        this.seesion_key_data = localStorage.getItem("key_session");  
         const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'Authorization': 'Token '+this.seesion_key_data,
          })
          };
         this.http.post<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/logout/", httpOptions)
          .subscribe(data => 
          {          
            if(JSON.stringify(data.detail))
            {
              localStorage.removeItem("key_session");
              this.seesion_key_data = null;
              this.router.navigate(['logout']);
            }
          },
           error => 
           {
            console.log("Error " + JSON.stringify(error));           
           }
          );
      }
     

load_user_data()
 {
  this.seesion_key_data = localStorage.getItem("key_session");
  console.log("seesion_key_data " + this.seesion_key_data);      
    const httpOptions = {
         headers: new HttpHeaders({
           'Content-Type':  'application/json',
           'Authorization': 'Token '+this.seesion_key_data,
         })
         };
        this.http.get<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/user/", httpOptions)
         .subscribe(data =>
         {
           this.firstname_val = JSON.stringify(data.first_name).replace(/\"/g, "");
           this.lastname_val = JSON.stringify(data.last_name).replace(/\"/g, "");
           this.fname=this.firstname_val.split("", 1);
           this.lname=this.lastname_val.split("", 1);
          },
         );
  }

      onGoToSearchHistory()
      {
        this.router.navigate(['searchhistory']);
      }

      goToProfile()
      {
         this.router.navigate(['profile']);
      }

      goToHome()
      {
         this.router.navigate(['home']);
      }
      onGoToExtendedsearch()
      {
        this.router.navigate(['extendedsearch']);
      }

      update(){
        this.load_user_data();
      }
}

