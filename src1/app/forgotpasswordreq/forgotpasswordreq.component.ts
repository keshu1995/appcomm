import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-forgotpasswordreq',
  templateUrl: './forgotpasswordreq.component.html',
  styleUrls: ['./forgotpasswordreq.component.sass']
})
export class ForgotpasswordreqComponent implements OnInit {

	forgotPassword_req: FormGroup;
	returnUrl: string;
	loading = false;
	submitted = false;
  seesion_key_data="";

  constructor(private formBuilder: FormBuilder,private http: HttpClient,private router: Router,) { }

  ngOnInit() {

     this.seesion_key_data = localStorage.getItem("key_session");  
         console.log("seesion_key_data"+this.seesion_key_data);
         if(this.seesion_key_data )
         {
          console.log("if condition search history");
          this.router.navigate(['home']);
         }

  	 this.forgotPassword_req = this.formBuilder.group({
            exampleInputEmail1: ['', Validators.required]
        });
  }

  get f() { return this.forgotPassword_req.controls; }

   onSubmit_forgotPassword_req() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.forgotPassword_req.invalid) {
            return;
        }
        else
        {
           const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json'
          })
          };


          var body = {
            'email':this.f.exampleInputEmail1.value,
                      };


           var v = this.http.post<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/password/reset/",body, httpOptions)
          .subscribe(data => 
          {

            if(JSON.stringify(data.status) == "200")
            {
               // console.log("sucees>>"+JSON.stringify(data.body.detail));
               // console.log(">>>>"+JSON.stringify(data.body.detail).replace(/\"/g, ""));
               this.router.navigate(['resetpassword']);
            }

          },
           error => 
           {
            console.log("Error11>>" + JSON.stringify(error));
            console.log("Error>>" + JSON.stringify(error.error.email[0]));
            console.log("Error>>" + JSON.stringify(error.status));
           }
          );

           this.router.navigate(['resetpassword']);
        }

    }

    goToHome()
    {
       this.router.navigate(['login']);
    }


}
