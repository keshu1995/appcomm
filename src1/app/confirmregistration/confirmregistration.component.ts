import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-confirmregistration',
  templateUrl: './confirmregistration.component.html',
  styleUrls: ['./confirmregistration.component.sass']
})
export class ConfirmregistrationComponent implements OnInit {

  seesion_key_data = "";
  constructor(private router: Router) { }

  ngOnInit() {
    this.seesion_key_data = localStorage.getItem("key_session");  
         console.log("seesion_key_data"+this.seesion_key_data);
         if(this.seesion_key_data )
         {
          this.router.navigate(['home']);
         }
  }

  goToHome1()
  {
  	this.router.navigate(['home']);
  }

}
