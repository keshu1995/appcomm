import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient,HttpClientModule,HttpParams,HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.sass']
})
export class RegistrationComponent implements OnInit {

	registrationForm: FormGroup;
	returnUrl: string;
	loading = false;
	submitted = false;
  resonseError = "";
  seesion_key_data="";

  register_token = "";

  constructor(private formBuilder: FormBuilder,private http: HttpClient,private router: Router,private route: ActivatedRoute,) {

     // const navigation = this.router.getCurrentNavigation().extras.name;
     // console.log(navigation);
   }

  ngOnInit() {

    console.log("register token"+this.route.snapshot.params.token);
    this.register_token = this.route.snapshot.params.token;

    if(this.register_token)
    {

          const httpOptions1 = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json'
          })
          };


          var body1 = {
            'key':this.register_token
                      };


           var v = this.http.post<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/registration/verify-email/",body1, httpOptions1)
          .subscribe(data => 
          {
            console.log("sucees>>"+JSON.stringify(data));

            this.router.navigate(['login']);
            
          },
           error => 
           {
          
            if(JSON.stringify(error.status) == "404")
            {
              //var key = JSON.stringify(error.error.email["0"]).replace(/\"/g, "");
              this.resonseError = JSON.stringify(error.statusText).replace(/\"/g, "");
              console.log("Error>>" + JSON.stringify(this.resonseError));
              window.alert(this.resonseError);
            }
          }
          );
    }

    //console.log("token rohit"+this.route.snapshot.params.token);

     // this.route.paramMap.subscribe(params => {
     //   console.log(params)
     //  });

     this.seesion_key_data = localStorage.getItem("key_session");  
         console.log("seesion_key_data"+this.seesion_key_data);
         if(this.seesion_key_data )
         {
          console.log("if condition search history");
          this.router.navigate(['home']);
         }

        this.registrationForm = this.formBuilder.group({
            inputEmail4: ['', Validators.required],
            inputPassword4: ['', Validators.required],
            inputAddress: ['', Validators.required],
            inputAddress2: ['',[Validators.required, Validators.minLength(8)]],
            inputAddress3: ['', Validators.required],
            customCheck: ['', Validators.required]
        });

        //let param1 = this.route.snapshot.queryParams["param1"];
  }



  get f() { return this.registrationForm.controls; }



  onSubmit_reg() {

       
         
       console.log("checkbox value >> "+this.f.customCheck.value);
       this.resonseError = "";
  
        this.submitted = true;
        // stop here if form is invalid
        if (this.registrationForm.invalid) {
            return;
        }
        else if(this.f.inputAddress2.value !== this.f.inputAddress3.value)
        {
          return;
        }
        else if(this.f.customCheck.value == "false" || this.f.customCheck.value == false)
        {
          return;
        }
        else
        {
          
           console.log("checkbox value"+this.f.customCheck.value);

          const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json'
          })
          };


          var body = {
            'email':this.f.inputAddress.value,
                                        'password1' : this.f.inputAddress2.value,
                                        'password2': this.f.inputAddress3.value,
                                        'first_name' : this.f.inputEmail4.value,
                                        'last_name': this.f.inputPassword4.value,
                                        'has_agreed_to_tos':'true'
                                      };


           var v = this.http.post<any>("https://acc-lookup.sjerlok.com:8081/api/accounts/registration/",body, httpOptions)
          .subscribe(data => 
          {
            console.log("sucees>>"+JSON.stringify(data));

            this.router.navigate(['confirmregistration']);
            // if(JSON.stringify(data.status) == "200")
            // {
            //   this.router.navigate(['login']);
            //   console.log("sucees>>"+JSON.stringify(data));
            // }

            // if(JSON.stringify(data.status) == "201")
            // {
            //   this.router.navigate(['login']);
            //   console.log("sucees>>"+JSON.stringify(data));
            // }
          },
           error => 
           {
            console.log("Error>>" + JSON.stringify(error.error.email[0]));
            console.log("Error>>" + JSON.stringify(error.status));

            if(JSON.stringify(error.status) == "400")
            {
              //var key = JSON.stringify(error.error.email["0"]).replace(/\"/g, "");
              this.resonseError = JSON.stringify(error.error.email["0"]).replace(/\"/g, "");
            }
          }
          );
        }
    }




       

}
